/*
 Name:		RedCarpetServer.ino
 Created:	28.09.2019 13:36:50
 Author:	Thk
*/

// the setup function runs once when you press reset or power the board


#include <WiFiUdp.h>
#include <ESP8266WiFiAP.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>   // Include the WebServer library

#include "secrets.h"
#define TO_HEX(i) (i <= 9 ? '0' + i : 'A' - 10 + i)

int Zaehler = 0;
byte maxProgNr = 5;
long oldMillis = 0;
byte ProgNr = 2;
byte selectProg = 0;
long ProgChangeTime = 90000;
byte Helligkeit = 50;
// Current time
unsigned long currentTime = millis();
// Previous time
unsigned long previousTime = 0;
// Define timeout time in milliseconds (example: 2000ms = 2s)
const long timeoutTime = 2000;
// Variable to store the HTTP request
String header;
byte oldPrgNr = 99;
long StepCounter = 0;
long old_UdpTime = 0;
long Diff = 12;
struct Farbe
{
	byte r;
	byte g;
	byte b;
};
struct Farbe LEDPIX[48];
WiFiServer server(80);
WiFiUDP    udp;


void setup() {
	Serial.begin(115200);
	Serial.print("Connecting to ");
	

	WiFi.mode(WIFI_AP);           //Only Access point
	WiFi.softAP(SECRET_SSID, SECRET_PASS);  //Start HOTspot removing password will disable security
	Serial.print("HotSpt IP:");
	Serial.print("Setting soft-AP configuration ... ");
	Serial.println(WiFi.softAPConfig(local_IP, gateway, subnet) ? "Ready" : "Failed!");


	Serial.print("Setting soft-AP ... ");
	Serial.println(WiFi.softAP(SECRET_SSID,SECRET_PASS) ? "Ready" : "Failed!");

	Serial.print("Soft-AP IP address = ");
	Serial.println(WiFi.softAPIP());
	IPAddress myIP = WiFi.softAPIP(); //Get IP address
	Serial.println(myIP);
	
	udp.begin(3333);
	
	server.begin();                           // Actually start the server
	Serial.println("HTTP server started");

	delay(45000);
	Serial.println("Start");
}

void loop() {
	
	long now = millis();

	if (now - old_UdpTime>Diff) {
		Serial.println("Trigger");
		Diff = 270000;
		old_UdpTime = now;
		udp.beginPacket(udpAddress, udpPort);
		String Temp = String(Helligkeit);

		if (Helligkeit < 100) { Temp = "0" + Temp; }
		if (Helligkeit < 10) { Temp = "0" + Temp; }
		Temp += ";" + String(StepCounter);
		String PRG = "#" + String(ProgNr) + Temp;
		char buf1[100];
		PRG.toCharArray(buf1, 100);

		udp.printf(buf1);
		udp.endPacket();

		delay(5);
		udp.printf(buf1);
		udp.endPacket();
		udp.beginPacket(udpAddress, udpPort);
		delay(5);
		udp.printf(buf1);
		udp.endPacket();
		udp.beginPacket(udpAddress, udpPort);
		StepCounter++;

	}
	
	delay(10);


	
	
}

void handleWeb() {
	WiFiClient client = server.available();   // Listen for incoming clients

	if (client) {                             // If a new client connects,
		Serial.println("New Client.");          // print a message out in the serial port
		String currentLine = "";                // make a String to hold incoming data from the client
		currentTime = millis();
		previousTime = currentTime;
		while (client.connected() && currentTime - previousTime <= timeoutTime) { // loop while the client's connected
			currentTime = millis();
			if (client.available()) {             // if there's bytes to read from the client,
				char c = client.read();             // read a byte, then
				Serial.write(c);                    // print it out the serial monitor
				header += c;
				if (c == '\n') {                    // if the byte is a newline character
				  // if the current line is blank, you got two newline characters in a row.
				  // that's the end of the client HTTP request, so send a response:
					if (currentLine.length() == 0) {
						// HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
						// and a content-type so the client knows what's coming, then a blank line:
						client.println("HTTP/1.1 200 OK");
						client.println("Content-type:text/html");
						client.println("Connection: close");
						client.println();

						// turns the GPIOs on and off
						if (header.indexOf("GET /prg") >= 0) {
							selectProg++;
							if (selectProg > maxProgNr) {
								selectProg = 0;
							}
						}
						else if (header.indexOf("GET /speed+") >= 0) {
							ProgChangeTime += 10000;
						}
						else if (header.indexOf("GET /speed-") >= 0) {

							ProgChangeTime -= 10000;
							if (ProgChangeTime < 10000) {
								ProgChangeTime = 10000;
							}
						}
						else if (header.indexOf("GET /light+") >= 0) {
							Helligkeit += 10;
							if (Helligkeit > 250) {
								Helligkeit = 250;
							}
						}
						else if (header.indexOf("GET /light-") >= 0) {
							Helligkeit -= 10;
							if (Helligkeit < 10) {
								Helligkeit = 10;
							}
						}

						// Display the HTML web page
						client.println("<!DOCTYPE html><html>");
						client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
						client.println("<link rel=\"icon\" href=\"data:,\">");
						// CSS to style the on/off buttons 
						// Feel free to change the background-color and font-size attributes to fit your preferences
						client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
						client.println(".button { background-color: #195B6A; border: none; color: white; padding: 16px 40px;");
						client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
						client.println(".button2 {background-color: #77878A;}</style></head>");

						// Web Page Heading
						client.println("<body><h1>ESP8266 Web Server</h1>");

						// Display current state, and ON/OFF buttons for GPIO 5  

						// If the output5State is off, it displays the ON button       
						if (selectProg != 0) {
							client.println("<p><a href=\"/prg\"><button class=\"button\">Programm " + String(selectProg) + "</button></a></p>");
						}
						else {
							client.println("<p><a href=\"/prg\"><button class=\"button\">Auto</button></a></p>");
						}
						client.println("<p><a href=\"/speed+\"><button class=\"button\">AUTO CHANGE TIME +</button></a></p>");
						
						
						client.println("<p><a href=\"/speed-\"><button class=\"button\">AUTO CHANGE TIME -</button></a></p>");
						client.println("<h1>" + String(ProgChangeTime / 1000) + " sec</h1>");
						client.println("<p><a href=\"/light+\"><button class=\"button\">Helligkeit +</button></a></p>");
						
						client.println("<p><a href=\"/light-\"><button class=\"button\">Helligkeit -</button></a></p>");
						client.println("<h1>" + String(Helligkeit) + "</h1>");
						// Display current state, and ON/OFF buttons for GPIO 4  

						client.println("</body></html>");

						// The HTTP response ends with another blank line
						client.println();
						// Break out of the while loop
						break;
					}
					else { // if you got a newline, then clear currentLine
						currentLine = "";
					}
				}
				else if (c != '\r') {  // if you got anything else but a carriage return character,
					currentLine += c;      // add it to the end of the currentLine
				}
			}
		}
		// Clear the header variable
		header = "";
		// Close the connection
		client.stop();
		Serial.println("Client disconnected.");
		Serial.println("");




	}
}

	




/*
 * This server waits for a client to connect and select an option from the buttons, for example if the client/user selects
 * the 'Client 1 Device ON. button, the UDP message sent will be 'C01H' to all clients. When a 'Client' receives the message C01H (all clients receive the same message!
 * It will match the address e.g. C01 to itself and act on the command H or L or ON or OFF and switch ON the corresponding Device and so-on for the other clients.
 */
