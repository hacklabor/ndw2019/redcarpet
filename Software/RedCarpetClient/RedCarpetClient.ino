/*
 Name:		RedCarpetClient.ino
 Created:	28.09.2019 14:39:28
 Author:	Thk
*/

// the setup function runs once when you press reset or power the board
#include <WiFiUdp.h>
#include <ESP8266WiFi.h>

#include <Adafruit_NeoPixel.h>

#include "secrets.h"

extern "C" {
#include "user_interface.h" //to set the hostname
}
IPAddress ip1(192, 168, 0,3);
IPAddress gateway1(192, 168, 0, 1);
IPAddress subnet1(255, 255, 255, 0);

WiFiUDP Udp;
char packetBuffer[255];
int brightness = 60;
int neopixelspeed = 70;
bool Takt = false;
byte PrgNr = 1;
int zaehler = 0;
byte oldPrgNr = 0;
long lastTakt = 0;
int wifi_retry = 0;
int aufuellen = 48;
int colorwipe = 0;
int cw_color = 0;
int rainbowC = 0;
int loopCounter = 0;
long startTime = 0;
long ProgTime = 0;
bool waitForUdp = true;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, NeoPIN, NEO_RGB + NEO_KHZ800);

struct Farbe
{
	byte r;
	byte g;
	byte b;
};
struct Farbe Wipe[10];





void setup()
{

	setColor(0, 255, 0, 0);
	setColor(1, 255, 255, 0);
	setColor(2, 0, 255, 0);
	setColor(3, 0, 255, 255);
	setColor(4, 0, 0, 255);
	setColor(5, 255, 0, 255);

	Serial.begin(115200);
	delay(500);
	Serial.println("StartWifi");

	WiFi.config(ip1, gateway1, subnet1);
	WiFi.begin(SECRET_SSID, SECRET_PASS);
	if (WiFi.waitForConnectResult() != WL_CONNECTED) {
		Serial.println("WiFi Failed");
		while (1) {
			delay(500);
			ESP.restart();
		}
	}
	Udp.begin(udpPort);
	//udp.broadcast("Device operated correctly");
	Serial.println("INIT PIXEL");
	strip.setBrightness(brightness);
	strip.begin();

	
	delay(50);
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());
	lastTakt = millis();
}

void loop()

{

	
	String str;

	
	
	
		
		if (Udp.parsePacket() && waitForUdp) {
			int len = Udp.read(packetBuffer, 255);
			if (len > 0) {

				packetBuffer[len] = 0;
				str = String(packetBuffer);

				if (packetBuffer[0] == 35) {
					startTime = millis();
					ProgTime = startTime;
					waitForUdp = false;
					Serial.println("StartUDP");
					Serial.println(startTime);
				}


			}
		}
	


	if ((millis()-lastTakt>29) && (startTime != 0 )) {
		lastTakt = millis();
		
		if (oldPrgNr != PrgNr) {
			Serial.println("ProgNummer" + String(PrgNr));
			Serial.println(millis() - ProgTime);
			ProgTime = millis();
			zaehler = 0;
			aufuellen = NUM_LEDS;
			colorwipe = 0;
			oldPrgNr = PrgNr;
			cw_color = 0;
			rainbowC = 0;
			loopCounter = 0;
			alloff();
		}
		if (PrgNr == 6) {
			Serial.println(millis() - startTime);
			delay(1000);
			ESP.restart();
		}

		Takt = false;
		switch (PrgNr) {
		case 1:
			rainbowCycle(40);
			break;
		case 2:
			LauflichtAufuellen(2, 255, 255, 255, 0, 255, 0);

			break;
		case 3:
			Lauflicht(15,0, 0, 0, 255, 0, 0);

			break;
		case 4:
			Lauflicht(15,0, 255, 0, 255, 0, 0);
			break;
		case 5:
			colorWipe(15,Wipe[cw_color].r, Wipe[cw_color].g, Wipe[cw_color].b);
			break;
		case 6:

			break;
		case 7:
			
			break;


		}
	
	}

}



void Lauflicht(int Anzahl, byte br, byte bg, byte bb, byte fr, byte fg, byte fb) {
	for (int i = 0; i < NUM_LEDS; i++) { strip.setPixelColor(i, bb, bg, br); }
	strip.setPixelColor(NUM_LEDS - 1 - zaehler, fb, fg, fr);
	zaehler++;
	if (zaehler > NUM_LEDS - 1) { 
		zaehler = 0; 
		loopCounter++;
		if (loopCounter >= Anzahl) { PrgNr++; }
	}
	strip.show();
}

void LauflichtAufuellen(int Anzahl, byte br, byte bg, byte bb, byte fr, byte fg, byte fb) {
	alloff();
	for (int i = 0; i < NUM_LEDS - aufuellen; i++) { strip.setPixelColor(i, bb, bg, br); }
	strip.setPixelColor(NUM_LEDS - 1 - zaehler, fb, fg, fr);
	zaehler++;
	if (zaehler > aufuellen - 1) {
		zaehler = 0; aufuellen--;
		if (aufuellen == 0) { aufuellen = NUM_LEDS; }
		loopCounter++;
		if (loopCounter >= Anzahl*NUM_LEDS) { PrgNr++; }
	}
	strip.show();
}


void colorWipe(int Anzahl,byte r, byte g, byte b) {


	strip.setPixelColor(NUM_LEDS - colorwipe, b, g, r);
	strip.show();
	colorwipe++;
	if (colorwipe > strip.numPixels()) {
		colorwipe = 0;
		alloff();
		cw_color++;
		if (cw_color > 5) { cw_color = 0; }
		loopCounter++;
		if (loopCounter >= Anzahl) { PrgNr++; }
	}

}











void alloff() {

	for (int i = 0; i < strip.numPixels(); i++) {
		strip.setPixelColor(i, 0, 0, 0);
	}

}


void setNeoColor(String value) {
	int number = (int)strtol(&value[1], NULL, 16);

	int r = number >> 16;
	int b = number >> 8 & 0xFF;
	int g = number & 0xFF;

	Serial.print("RGB: ");
	Serial.print(r, DEC);
	Serial.print(" ");
	Serial.print(g, DEC);
	Serial.print(" ");
	Serial.print(b, DEC);
	Serial.println(" ");

	for (int i = 0; i < NUM_LEDS; i++) {
		strip.setPixelColor(i, strip.Color(g, r, b));
	}
	strip.show();

}


// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
	WheelPos = 255 - WheelPos;
	if (WheelPos < 85) {
		return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
	}
	if (WheelPos < 170) {
		WheelPos -= 85;
		return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
	}
	WheelPos -= 170;
	return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

void setColor(int nummer, byte r, byte g, byte b) {
	Wipe[nummer].r = r;
	Wipe[nummer].g = g;
	Wipe[nummer].b = b;
}

void rainbowCycle(int Anzahl) {
	uint16_t i, j;


	for (i = 0; i < strip.numPixels(); i++) {
		strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + rainbowC) & 255));
	}
	strip.show();
	rainbowC++;
	loopCounter++;
		if (loopCounter >= Anzahl*NUM_LEDS) { PrgNr++; }

}
